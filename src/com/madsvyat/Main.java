package com.madsvyat;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    public static void main(String[] args) {

        BlockingQueue<Integer> randomsQueue = new LinkedBlockingQueue<>(128);
        Set<Integer> results = Collections.synchronizedSet(new HashSet<>());

        RandomGenerator generator = new RandomGenerator(randomsQueue);
        RandomAggregator aggregator = new RandomAggregator(randomsQueue, results);

        Thread generatorThread = new Thread(generator);
        Thread aggregatorThread = new Thread(aggregator);

        generatorThread.start();
        aggregatorThread.start();

        while (results.size() < 100) {
            Thread.yield();
        }

        generator.finish();
        aggregator.finish();

        System.out.println("finished");
    }
}
