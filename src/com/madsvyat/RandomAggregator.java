package com.madsvyat;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;

/**
 *
 */
public class RandomAggregator implements Runnable {


    private volatile boolean finished;
    private BlockingQueue<Integer> randomsQueue;
    private Set<Integer> uniqueValues = new HashSet<>();

    public RandomAggregator(BlockingQueue<Integer> randomsQueue, Set<Integer> uniqueValues) {
        this.randomsQueue = randomsQueue;
        this.uniqueValues = uniqueValues;
    }

    @Override
    public void run() {
        while (!finished) {
            try {
                Thread.sleep(5000);

                while (!randomsQueue.isEmpty()) {
                    Integer nextRandom = randomsQueue.poll();
                    if (nextRandom != null) {
                        uniqueValues.add(nextRandom);
                    }
                }

                System.out.println("Generated: " + uniqueValues.size() + " unique values");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void finish() {
        finished = true;
    }
}
