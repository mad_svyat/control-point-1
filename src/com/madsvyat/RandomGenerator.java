package com.madsvyat;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

/**
 *
 */
public class RandomGenerator implements Runnable {

    private volatile boolean finished;
    private Random random;
    private BlockingQueue<Integer> randomsQueue;

    public RandomGenerator(BlockingQueue<Integer> randomsQueue) {
        this.randomsQueue = randomsQueue;
        this.random = new Random();
    }

    @Override
    public void run() {
        while (!finished) {
            int value = random.nextInt(100);

            try {
                randomsQueue.put(value);

                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void finish() {
        finished = true;
    }
}
